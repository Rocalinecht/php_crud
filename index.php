<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PHP Crud</title>
</head>
<body>
<h1>Exercice PHP Crud</h1>
<?php
    $bdd = new PDO("mysql:host=127.0.0.1;dbname=colyseum","root","root");

    ##__________________________________________________-->Exercice 1 : Afficher tous les clients.
    $requeteClient = $bdd->query("SELECT * FROM clients ");
    while($client = $requeteClient->fetch()){

       // echo $client['lastName']." ".$client['firstName']."<br/>";
    }
    ##__________________________________________________-->Exercice 2 : Afficher tous les types de spectacles possibles.
    $requeteSpectacle = $bdd->query("SELECT * FROM  showTypes");
    while($Spectacle = $requeteSpectacle->fetch()){

        //echo $Spectacle['type']."<br/>";
    }
    ##___________________________________________________Exercice 3 : Afficher les 20 premiers clients.
    $req20Client = $bdd->query("SELECT * FROM clients LIMIT 20");
    while($client20 = $req20Client->fetch()){

       //echo $client20['lastName']." ".$client20['firstName']."<br/>";
    }
    ##______________________________Exercice 4 : N'afficher que les clients possédant une carte de fidélité.
    $reqClientCard = $bdd->query("SELECT * FROM clients WHERE card='1'");
    while($clientCard = $reqClientCard->fetch()){

       //echo $clientCard['lastName']." ".$clientCard['firstName']."<br/>";
    }
    ##Exercice 5______Afficher uniquement le nom et le prénom de tous les clients dont le nom commence par la lettre "M".
    $reqClientM = $bdd->query("SELECT * FROM clients WHERE lastName LIKE 'M%' ORDER BY lastName");
    
    while($clientM = $reqClientM->fetch()){

       //echo "Nom :".$clientM['lastName']."<br/>"."Prenom : ".$clientM['firstName']."<br/>";
    }
    ##Exercice 6
    //Afficher le titre de tous les spectacles ainsi que l'artiste, la date et l'heure.
    // Trier les titres par ordre alphabétique.
    //Afficher les résultat comme ceci : *Spectacle* par *artiste*, le *date* à *heure*.
    $reqShow = $bdd->query("SELECT * FROM shows ORDER BY title ");
    while($show = $reqShow->fetch()){
       // echo $show['title']." par ".$show['performer']." ,le ".$show['date']." à ".$show['startTime']."<br/>";
       
    }
    ##Exercice 7
    // Afficher tous les clients comme ceci :
    // Nom : *Nom de famille du client*
    // Prénom : *Prénom du client*
    // Date de naissance : *Date de naissance du client*
    // Carte de fidélité : *Oui (Si le client en possède une) ou Non (s'il n'en possède pas)*
    // Numéro de carte : *Numéro de la carte fidélité du client s'il en possède une.*
    $reqAllClient = $bdd->query("SELECT * FROM clients ");
    while($Allclient = $reqAllClient->fetch()){

       echo  "Nom : ".$Allclient['lastName']."<br/>"
            ."Prenom : ".$Allclient['firstName']."<br/>"
            ."Date de naissance : ".$Allclient['birthDate']."<br/>";
            // ."Carte de fidelité : ".$Allclient['card']."<br/>"
            // ."Numero de carte : ".$Allclient['cardNumber']."<br/>";

        if($Allclient['card'] === '1'){
            echo "Carte de fidelité :  Oui "."<br/>"
                ."Numero de carte : ".$Allclient['cardNumber']."<br/>";
        }
        elseif($Allclient['card'] === '0'){
            echo "Carte de fidelité :  Non "."<br/>";
        }
    }

?>
</body>
</html>